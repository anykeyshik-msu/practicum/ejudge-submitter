#!/bin/bash

WARNING='\033[1;31m'
INPUT='\033[1;33m'
INFO='\033[1;32m'
NC='\033[0m'

CONTEST_DIR="/tmp/contests"
FUSE_BIN="/tmp/ej-fuse/src"

echo -e "${INFO}Welcome to task submitter for unicorn.ejudge!${NC}"

if [ -z "$(ls -A $CONTEST_DIR)" ]; then
    echo -ne "${INPUT}Please enter your ejduge login: ${NC}"
    read login
    echo -en "${INPUT}Please enter your ejudge password: ${NC}"
    read -s password

    echo -e "\n${INFO}Make dir for mounting ejudge${NC}"
    mkdir -p $CONTEST_DIR

    echo -e "${INFO}Mounting ejudge${NC}"
    if ! $FUSE_BIN/ejudge-fuse --user $login --password $password --url https://unicorn.ejudge.ru/cgi-bin/ $CONTEST_DIR -o use_ino; then
        echo -e "${WARNING}Problem with ejudge mounting. Please check your login and password${NC}"
        exit
    fi
fi

echo -ne "${INPUT}Please enter filename for submit: ${NC}"
read filename
echo -ne "${INPUT}Please enter problem for submit: ${NC}"
read problem

CONTEST_ID=$(ls $CONTEST_DIR | cut -d ',' -f 1)
$(cp $filename $CONTEST_DIR/$CONTEST_ID/problems/$task/submit/0)

echo -e "${INFO}Task submitted successfully!${NC}"

echo -e "${INFO}Clean all${NC}"
fusermount -u $CONTEST_DIR
